package club.anims.manuma;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

public class API {
    private static String url = "http://alastor.overmc.net:34000/api";
    private static OkHttpClient client = new OkHttpClient();

    public static String callPost(String endpoint, String json){
        var output = "";
        var body = RequestBody.create(
                MediaType.parse("application/json"), json
        );
        var request = new Request.Builder()
                .url(String.format("%s%s",url,endpoint))
                .post(body)
                .build();
        var call = client.newCall(request);
        try{
            var response = call.execute();
            output = response.body().string();
        }catch (Exception ex){
            output = null;
        }
        return output;
    }


}
