package club.anims.manuma;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class App extends Application {

    protected static Stage stage;

    public static App app;

    public void loadScene(Page page){
        switch (page){
//            case TASKS -> loadScene("tasks");
            case ADD_CATEGORY -> loadScene("createCategory");
            case ADD_TASK -> loadScene("createTask");
//            case CATEGORIES -> loadScene("categories");
            case MAIN_PAGE -> loadScene("mainPage");
            case TASK_INFO -> loadScene("taskInfo");
            default -> loadScene("login");
        }
    }

    private void loadScene(String name){
        try{
            var page = getClass().getClassLoader().getResource(String.format("%s.fxml",name));
            Parent parent = FXMLLoader.load(page);
            var scene = new Scene(parent,640,480);
            stage.setScene(scene);
            stage.show();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    @Override
    public void start(Stage _stage) {
        stage = _stage;
        app = this;
        loadScene(Page.LOGIN);
    }

    public static void main(String[] args){
        launch();
    }
}
