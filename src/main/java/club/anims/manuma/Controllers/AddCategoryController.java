package club.anims.manuma.Controllers;

import club.anims.manuma.*;
import club.anims.manuma.DbObjects.Category;
import club.anims.manuma.DbObjects.Task;
import club.anims.manuma.DbObjects.User;
import club.anims.manuma.Requests.AddCategoryRequest;
import club.anims.manuma.Requests.GetTasksRequest;
import club.anims.manuma.Responses.DefaultResponse;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

import java.util.List;

import static club.anims.manuma.JsonUtils.jackson;
import static club.anims.manuma.Utils.colorToHex;

public class AddCategoryController {

    @FXML
    public TextField name;

    @FXML
    public TextArea description;

    @FXML
    public ColorPicker colorPicker;

    @FXML
    public Button create;

    @FXML
    public FontAwesomeIconView leave;

    @FXML
    public void onButtonCreateClick(ActionEvent e){
        new RunnableProxy(run -> {
            try{
                var color = colorToHex(colorPicker.getValue());
                var category = new Category(0,Data.user.id, name.getText(), color, -1, description.getText());
                category.color = color;
                var request = new AddCategoryRequest(Data.user.email, Data.user.password, category);
                var json = jackson().writeValueAsString(request);
                var response = new Gson().fromJson(API.callPost("/addcategory", json), DefaultResponse.class);
                if(response.response){
                    var alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("From server:");
                    alert.setContentText("Category was successfully created.");
                    alert.show();
                    App.app.loadScene(Page.MAIN_PAGE);
                }else{
                    var alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("From server:");
                    alert.setContentText("Error occurred whilst tried to create this category.");
                    alert.show();
                }

            }catch (Exception ex){
                ex.printStackTrace();
                var alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Connection error.");
                alert.setContentText("Error occurred whilst trying connecting to server;");
                alert.show();
            }
        });
    }

    @FXML
    public void onButtonLeaveClick(MouseEvent e){
        App.app.loadScene(Page.MAIN_PAGE);
    }
}
