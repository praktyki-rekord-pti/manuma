package club.anims.manuma.Controllers;

import club.anims.manuma.*;
import club.anims.manuma.DbObjects.Category;
import club.anims.manuma.DbObjects.Task;
import club.anims.manuma.Requests.AddCategoryRequest;
import club.anims.manuma.Requests.AddTaskRequest;
import club.anims.manuma.Responses.DefaultResponse;
import com.google.gson.Gson;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

import static club.anims.manuma.JsonUtils.jackson;
import static club.anims.manuma.Utils.colorToHex;
import static club.anims.manuma.Utils.localDateTimeToDateTime;

public class AddTaskController implements Initializable {
    @FXML
    public DatePicker deadline;

    @FXML
    public TextField name;

    @FXML
    public TextArea description;

    @FXML
    public ColorPicker colorPicker;

    @FXML
    public Button create;

    @FXML
    public FontAwesomeIconView leave;

    @FXML
    public ChoiceBox<String> choiceBox;


    @FXML
    public void onButtonCreateClick(ActionEvent e){
        new RunnableProxy(run -> {
            try{
                var color = colorToHex(colorPicker.getValue());
                var cat_id = Data.categories.get(choiceBox.getSelectionModel().getSelectedIndex()).id;
                var date = localDateTimeToDateTime(deadline.getValue().atTime(23,59,59));
                var task = new Task(0, cat_id, date, (short) 1,name.getText(), color, -1, description.getText());
                var request = new AddTaskRequest(Data.user.email, Data.user.password, task);
                var json = jackson().writeValueAsString(request);
                var response = new Gson().fromJson(API.callPost("/addtask", json), DefaultResponse.class);
                if(response.response){
                    var alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("From server:");
                    alert.setContentText("Task was successfully created.");
                    alert.show();
                    App.app.loadScene(Page.MAIN_PAGE);
                }else{
                    var alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("From server:");
                    alert.setContentText("Error occurred whilst tried to create this task.");
                    alert.show();
                }

            }catch (Exception ex){
                ex.printStackTrace();
                var alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Connection error.");
                alert.setContentText("Error occurred whilst trying connecting to server;");
                alert.show();
            }
        });
    }

    @FXML
    public void onButtonLeaveClick(MouseEvent e){
        App.app.loadScene(Page.MAIN_PAGE);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        Data.categories.forEach(o -> {
            choiceBox.getItems().add(o.name);
        });
        choiceBox.getSelectionModel().select(0);
    }
}
