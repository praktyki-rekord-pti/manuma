package club.anims.manuma.Controllers;

import club.anims.manuma.*;
import club.anims.manuma.DbObjects.User;
import club.anims.manuma.Requests.LoginRequest;
import club.anims.manuma.Responses.DefaultResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.Gson;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

import static club.anims.manuma.JsonUtils.jackson;

public class LoginController {
    @FXML
    public TextField email;

    @FXML
    public PasswordField password;

    @FXML
    public void onSignInButtonClick(ActionEvent e){
        new RunnableProxy(o -> {
            try{
                var request = new LoginRequest(email.getText(), password.getText());
                var json = jackson().writeValueAsString(request);
                var response = new Gson().fromJson(API.callPost("/login", json), DefaultResponse.class);
                if(response.response){
                    Data.user = new Gson().fromJson(API.callPost("/getuser", json), User.class);
                    App.app.loadScene(Page.MAIN_PAGE);
                }else{
                    var alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("From server:");
                    alert.setContentText("Provided credentials are not correct.");
                    alert.show();
                }
            }catch (Exception ex){
                var alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Connection error.");
                alert.setContentText("Couldn't connect to server.");
                alert.show();
            }
        });
    }
}
