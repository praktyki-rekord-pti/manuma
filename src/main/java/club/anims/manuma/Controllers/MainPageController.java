package club.anims.manuma.Controllers;

import club.anims.manuma.*;
import club.anims.manuma.DbObjects.Category;
import club.anims.manuma.DbObjects.Task;
import club.anims.manuma.Requests.GetTasksRequest;
import club.anims.manuma.Requests.LoginRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import static club.anims.manuma.JsonUtils.jackson;

public class MainPageController implements Initializable {
    private ArrayList<Category> categoryList;
    private ArrayList<Task> taskList;

    private void loadCategory(int id){
        if (!tasks.getItems().isEmpty()) tasks.getItems().clear();
        new RunnableProxy(run -> {
            try{
                long cat_id = 0;
                try{
                    cat_id = categoryList.get(id).id;
                }catch (Exception ex){
                    return;
                }
                var request = new GetTasksRequest(Data.user.email, Data.user.password, cat_id);
                var json = jackson().writeValueAsString(request);
                var response = API.callPost("/gettask", json);
                taskList = new Gson().fromJson(response, new TypeToken<List<Task>>(){}.getType());
                taskList.forEach(o -> {
                    tasks.getItems().add(String.format("%s",o.name));
                });
                tasks.getItems().add("Add new task");
            }catch (Exception ex){
                var alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Connection error.");
                alert.setContentText("Error occurred whilst trying connecting to server;");
                alert.show();
            }
        });
    }

    @FXML
    public ListView<String> categories;

    @FXML
    public ListView<String> tasks;

    @FXML
    public void onPlusClick(MouseEvent e){
        App.app.loadScene(Page.ADD_CATEGORY);
    }

    @FXML
    public void onCategoryClicked(MouseEvent e){
       loadCategory(categories.getSelectionModel().getSelectedIndex());
    }

    @FXML
    public void onTaskClicked(MouseEvent e){
        var task_id = tasks.getSelectionModel().getSelectedIndex();
        if (task_id>=tasks.getItems().size() - 1){
            Data.categories = categoryList;
            App.app.loadScene(Page.ADD_TASK);
            return;
        }
        new RunnableProxy(run -> {
            Data.task = taskList.get(task_id);
            App.app.loadScene(Page.TASK_INFO);
        });
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        new RunnableProxy(run -> {
            try{
                var request = new LoginRequest(Data.user.email, Data.user.password);
                var json = jackson().writeValueAsString(request);
                var response = API.callPost("/getcategories", json);
                categoryList = new Gson().fromJson(response, new TypeToken<List<Category>>(){}.getType());
                categoryList.forEach(o -> {
                    categories.getItems().add(String.format("%s",o.name));
                });
            }catch (Exception ex){
                ex.printStackTrace();
                var alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Connection error.");
                alert.setContentText("Error occurred whilst trying connecting to server;");
                alert.show();
            }
        });
        new TryProxy(t -> {
            loadCategory(0);
        });
    }
}
