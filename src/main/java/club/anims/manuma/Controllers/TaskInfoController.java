package club.anims.manuma.Controllers;

import club.anims.manuma.App;
import club.anims.manuma.Data;
import club.anims.manuma.Page;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ResourceBundle;

public class TaskInfoController implements Initializable {
    @FXML
    public Label name;

    @FXML
    public Label description;

    @FXML
    public Label category;

    @FXML
    public Label deadline;

    @FXML
    public void onPlusClick(MouseEvent e){
        App.app.loadScene(Page.MAIN_PAGE);
    }

    @FXML
    public void onDeleteClick(ActionEvent e){

    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        var task = Data.task;
        name.setText(String.format("Name: %s",task.name));
        category.setText(String.format("Category: %s",task.category_id));
        description.setText(String.format("Description: %s",task.description));
        var date = new SimpleDateFormat("yyyy-MM-dd");
        try {
            deadline.setText(String.format("Deadline: %s",date.parse(task.deadline).toString()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}