package club.anims.manuma;

import club.anims.manuma.DbObjects.Category;
import club.anims.manuma.DbObjects.Task;
import club.anims.manuma.DbObjects.User;
import javafx.scene.control.ListView;

import java.util.List;

public class Data {
    public static User user;
    public static Task task;
    public static List<Category> categories;
}
