package club.anims.manuma.DbObjects;

import club.anims.manuma.SQLObjects.Nvarchar;
import club.anims.manuma.SQLObjects.Varchar;
import com.google.gson.annotations.SerializedName;

public class Category {
    @SerializedName(value="id",alternate = {"Id","ID"})
    public long id;

    @SerializedName(value="user_id",alternate = {"User_Id","USER_ID"})
    public long user_id;

    @SerializedName(value="Name",alternate = {"name","NAME"})
    public String name;

    @SerializedName(value="color",alternate = {"Color","COLOR"})
    public String color;

    @SerializedName(value="position",alternate = {"Position","POSITION"})
    public int position;

    @SerializedName(value="description",alternate = {"Description","DESCRIPTION"})
    public String description;

    public Category(long id, long user_id, String name, String color, int position, String description){
        this.id = id;
        this.user_id = user_id;
        this.name = new Varchar(name).get(32);
        this.color = new Varchar(color).get(7);
        this.position = position;
        this.description = new Nvarchar(description).get(255);
    }
}
