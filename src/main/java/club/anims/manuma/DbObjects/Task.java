package club.anims.manuma.DbObjects;

import club.anims.manuma.SQLObjects.Nvarchar;
import club.anims.manuma.SQLObjects.Varchar;
import com.google.gson.annotations.SerializedName;

public class Task {
    @SerializedName(value="id",alternate = {"ID","Id"})
    public long id;

    @SerializedName(value="category_id",alternate = {"Category_Id","CATEGORY_ID"})
    public long category_id;

    @SerializedName(value="deadline",alternate = {"Deadline","DEADLINE"})
    public String deadline;

    @SerializedName(value="state",alternate = {"State","STATE"})
    public short state;

    @SerializedName(value="name",alternate = {"Name","NAME"})
    public String name;

    @SerializedName(value="color",alternate = {"Color","COLOR"})
    public String color;

    @SerializedName(value="position",alternate = {"Position","POSITION"})
    public int position;

    @SerializedName(value="description",alternate = {"Description","DESCRIPTION"})
    public String description;

    public Task(long id, long category_id, String deadline, short state, String name, String color, int position, String description){
        this.id = id;
        this.category_id = category_id;
        this.deadline = deadline;
        this.state = state;
        this.name = new Nvarchar(name).get(64);
        this.color = new Varchar(color).get(7);
        this.position = position;
        this.description = new Nvarchar(description).get(255);
    }
}
