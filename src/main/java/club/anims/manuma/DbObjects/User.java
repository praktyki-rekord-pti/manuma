package club.anims.manuma.DbObjects;

import club.anims.manuma.SQLObjects.Nvarchar;
import club.anims.manuma.SQLObjects.Varchar;
import com.google.gson.annotations.SerializedName;

public class User {
    @SerializedName(value="id",alternate = {"ID","Id"})
    public long id;

    @SerializedName(value="name",alternate = {"Name","NAME"})
    public String name;

    @SerializedName(value="email",alternate = {"Email","EMAIL"})
    public String email;

    @SerializedName(value="password",alternate = {"Password","PASSWORD"})
    public String password;

    public User(long id, String name, String email, String password){
        this.id = id;
        this.name = new Varchar(name).get(32);
        this.email = new Varchar(email).get(128);
        this.password = new Nvarchar(password).get(255);
    }
}
