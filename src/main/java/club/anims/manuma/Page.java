package club.anims.manuma;

public enum Page {
    LOGIN,
//    CATEGORIES,
//    TASKS,
    ADD_CATEGORY,
    ADD_TASK,
    MAIN_PAGE,
    TASK_INFO
}
