package club.anims.manuma.Requests;

import club.anims.manuma.DbObjects.Category;
import club.anims.manuma.SQLObjects.Nvarchar;
import club.anims.manuma.SQLObjects.Varchar;

public class AddCategoryRequest {
    public String email;
    public String password;
    public Category category;

    public AddCategoryRequest(String email, String password, Category category){
        this.email = new Varchar(email).get(128);
        this.password = new Nvarchar(password).get(255);
        this.category = category;
    }
}
