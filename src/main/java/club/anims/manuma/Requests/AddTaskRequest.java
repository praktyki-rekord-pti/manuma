package club.anims.manuma.Requests;

import club.anims.manuma.DbObjects.Task;
import club.anims.manuma.SQLObjects.Nvarchar;
import club.anims.manuma.SQLObjects.Varchar;

public class AddTaskRequest {
    public String email;
    public String password;
    public Task task;

    public AddTaskRequest(String email, String password, Task task){
        this.email = new Varchar(email).get(128);
        this.password = new Nvarchar(password).get(255);
        this.task = task;
    }
}
