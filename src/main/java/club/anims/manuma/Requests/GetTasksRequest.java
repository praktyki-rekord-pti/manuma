package club.anims.manuma.Requests;

import club.anims.manuma.SQLObjects.Nvarchar;
import club.anims.manuma.SQLObjects.Varchar;

public class GetTasksRequest {
    public String email;
    public String password;
    public long category_id;

    public GetTasksRequest(String email, String password, long category_id){
        this.email = new Varchar(email).get(128);
        this.password = new Nvarchar(password).get(255);
        this.category_id = category_id;
    }
}
