package club.anims.manuma.Requests;

import club.anims.manuma.SQLObjects.Nvarchar;
import club.anims.manuma.SQLObjects.Varchar;

public class LoginRequest {
    public String email;
    public String password;

    public LoginRequest(String email, String password){
        this.email = new Varchar(email).get(128);
        this.password = new Nvarchar(password).get(255);
    }
}
