package club.anims.manuma.Requests;

import club.anims.manuma.DbObjects.Category;
import club.anims.manuma.SQLObjects.Nvarchar;
import club.anims.manuma.SQLObjects.Varchar;

import java.util.ArrayList;

public class UpdateCategoriesRequest {
    public String email;
    public String password;
    public ArrayList<Category> categories = new ArrayList<>();

    public UpdateCategoriesRequest(String email, String password, ArrayList<Category> categories){
        this.email = new Varchar(email).get(128);
        this.password = new Nvarchar(password).get(255);
        this.categories.addAll(categories);
    }
}
