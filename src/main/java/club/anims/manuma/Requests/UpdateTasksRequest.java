package club.anims.manuma.Requests;

import club.anims.manuma.DbObjects.Task;
import club.anims.manuma.SQLObjects.Nvarchar;
import club.anims.manuma.SQLObjects.Varchar;

import java.util.ArrayList;

public class UpdateTasksRequest {
    public String email;
    public String password;
    public ArrayList<Task> tasks = new ArrayList<>();

    public UpdateTasksRequest(String email, String password, ArrayList<Task> tasks){
        this.email = new Varchar(email).get(128);
        this.password = new Nvarchar(password).get(255);
        this.tasks.addAll(tasks);
    }
}
