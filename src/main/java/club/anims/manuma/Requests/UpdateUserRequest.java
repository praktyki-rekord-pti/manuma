package club.anims.manuma.Requests;

import club.anims.manuma.DbObjects.User;
import club.anims.manuma.SQLObjects.Nvarchar;
import club.anims.manuma.SQLObjects.Varchar;

public class UpdateUserRequest {
    public String email;
    public String password;
    public User user;

    public UpdateUserRequest(String email, String password, User user){
        this.email = new Varchar(email).get(128);
        this.password = new Nvarchar(password).get(255);
        this.user = user;
    }
}
