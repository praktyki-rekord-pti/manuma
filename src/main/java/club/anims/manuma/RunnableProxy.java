package club.anims.manuma;

import javafx.application.Platform;

import java.util.function.Consumer;

public class RunnableProxy {
    private Consumer<Object> o;

    public RunnableProxy(Consumer<Object> o){
        this.o = o;
        repeat();
    }

    public void repeat(){
        var runnable = new Runnable(){
            @Override
            public void run() {
                o.accept("");
            }
        };
        Platform.runLater(runnable);
    }
}
