package club.anims.manuma.SQLObjects;

public class Nvarchar {
    private final String string;

    private String trim(String string, int maxLength){
        var output = string;
        if(output.length()>maxLength) output = output.substring(0, maxLength);
        return output;
    }

    public Nvarchar(String string){
        this.string = string;
    }

    public Nvarchar(String string, int maxLength){
        this.string = trim(string, maxLength);
    }

    public String get(int maxLength){
        return trim(this.string, maxLength);
    }

    public String get(){
        return this.string;
    }
}
