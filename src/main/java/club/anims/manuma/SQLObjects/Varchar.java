package club.anims.manuma.SQLObjects;

public class Varchar extends Nvarchar{

    public Varchar(String string) {
        super(string.replaceAll("[^\\x00-\\x7F]", ""));
    }

    public Varchar(String string, int maxLength) {
        super(string.replaceAll("[^\\x00-\\x7F]", ""),maxLength);
    }

    @Override
    public String get() {
        return super.get();
    }

    @Override
    public String get(int maxLength) {
        return super.get(maxLength);
    }
}
