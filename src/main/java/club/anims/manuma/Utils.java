package club.anims.manuma;

import club.anims.manuma.DbObjects.Category;
import javafx.scene.paint.Color;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Utils {
    public static String localDateTimeToDateTime(LocalDateTime ldt){
        var output = new StringBuilder();
        output.append(ldt.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        output.append("T");
        output.append(ldt.format(DateTimeFormatter.ofPattern("HH:mm:ss.SSSSSSS")));
        output.append("+02:00");
        return new String(output);
    }

    public static String dateTimePlaceHolder = "{%Place$Holder%}";
    public static String replaceDateTimePlaceHolder(String json, LocalDateTime ldt){
        return json.replace(dateTimePlaceHolder,localDateTimeToDateTime(ldt));
    }

    public static String colorToHex(Color color){
        return String.format("#%02x%02x%02x", Math. round(color.getRed()*255), Math.round(color.getGreen()*255), Math.round(color.getBlue()*255));
    }
}
