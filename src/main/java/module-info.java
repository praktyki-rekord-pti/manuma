module club.anims.manuma {
    requires com.fasterxml.jackson.annotation;
    requires com.fasterxml.jackson.core;
    requires com.fasterxml.jackson.databind;
    requires com.fasterxml.jackson.datatype.jdk8;
    requires com.fasterxml.jackson.datatype.jsr310;
    requires com.fasterxml.jackson.module.paramnames;

    requires com.google.gson;
    requires kotlin.stdlib;
    requires okhttp3;
    requires org.apache.commons.codec;

    exports club.anims.manuma.SQLObjects;
    exports club.anims.manuma.Controllers;
    exports club.anims.manuma.DbObjects;
    exports club.anims.manuma.Requests;
    exports club.anims.manuma.Responses;
    exports club.anims.manuma;

    opens club.anims.manuma;

    requires javafx.base;
    requires javafx.controls;
    requires javafx.graphics;
    requires javafx.fxml;

    requires annotations;
    requires de.jensd.fx.glyphs.fontawesome;
}